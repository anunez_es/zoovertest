/**
 * 
 */
package com.anunez.zoovertest.adapters;

import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.anunez.zoovertest.CityActivity;
import com.anunez.zoovertest.widgets.Widget;
import com.terlici.dragndroplist.DragNDropAdapter;
import com.terlici.dragndroplist.DragNDropListView;

/**
 * @author Angel
 *
 */
public class DragNDropWidgetAdapter extends ArrayAdapter<Widget> implements
		DragNDropAdapter {
	private Context context;
	private int mPosition[];
	private int mHandler;// The view that can be dragged
	private List<Widget> dataList;

	public DragNDropWidgetAdapter(Context context, List<Widget> data,
			int resource, int handler) {
		super(context, resource, data);

		this.context = context;
		mHandler = handler;
		dataList = data;
		setup(data.size());

		LayoutInflater.from(context);
	}

	private void setup(int size) {
		mPosition = new int[size];

		for (int i = 0; i < size; ++i)
			mPosition[i] = i;
	}

	@Override
	public View getDropDownView(int position, View view, ViewGroup group) {
		return super.getDropDownView(mPosition[position], view, group);
	}

	@Override
	public Widget getItem(int position) {
		return super.getItem(mPosition[position]);
	}

	@Override
	public int getItemViewType(int position) {
		return super.getItemViewType(mPosition[position]);
	}

	@Override
	public long getItemId(int position) {
		return super.getItemId(mPosition[position]);
	}

	@Override
	public View getView(int position, View view, ViewGroup group) {
		ViewContainer container;
		// Can't recycle if the order can change
		Widget widget = dataList.get(mPosition[position]);
		widget.setPosition(position);
		widget.inflate();
		container = new ViewContainer(widget);
		widget.setTag(container);		
		return widget;
	}

	@Override
	public boolean isEnabled(int position) {
		return super.isEnabled(mPosition[position]);
	}

	@Override
	public void onItemDrag(DragNDropListView parent, View view, int position,
			long id) {

	}

	@Override
	public void onItemDrop(DragNDropListView parent, View view,
			int startPosition, int endPosition, long id) {
		int position = mPosition[startPosition];

		if (startPosition < endPosition)
			for (int i = startPosition; i < endPosition; ++i)
				mPosition[i] = mPosition[i + 1];
		else if (endPosition < startPosition)
			for (int i = startPosition; i > endPosition; --i)
				mPosition[i] = mPosition[i - 1];

		mPosition[endPosition] = position;
		
		//Save position changes in CityFragment
		if(context instanceof CityActivity){
			List<Widget> widgets = ((CityActivity)context).getVisibleCityFragment().getWidgets();
			widgets.get(startPosition).setPosition(endPosition);
			Collections.sort(widgets);
		}
	}

	@Override
	public int getCount() {
		return dataList.size();
	}

	@Override
	public void notifyDataSetChanged() {
		setup(dataList.size());
		super.notifyDataSetChanged();
	}

	@Override
	public int getDragHandler() {
		return mHandler;
	}

	public int[] getPositionsArray() {
		return mPosition;
	}

	public class ViewContainer {
		View view;

		public ViewContainer(View view) {
			this.view = view;
		}

	}
}
