/**
 * 
 */
package com.anunez.zoovertest.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * @author Angel
 *
 */
public class Utils {

	public static class Hours {
		public static String hour(int hour) {
			hour = hour%24;
			return hour < 10 ? "0" : "" + hour + ":00";
		}

		public static String hhmm(Calendar calendar) {
			return new SimpleDateFormat("HH:mm", Locale.UK).format(calendar
					.getTime());
		}
	}

}
