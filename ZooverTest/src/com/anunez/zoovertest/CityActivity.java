package com.anunez.zoovertest;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anunez.zoovertest.about.About;
import com.anunez.zoovertest.fragments.CityFragment;
import com.anunez.zoovertest.util.Utils.Hours;

public class CityActivity extends Activity {

	List<CityFragment> fragments = new ArrayList<CityFragment>();
	int visiblePosition = 0;
	LinearLayout citySelector;
	TextView lblCityName, lblLastUpdate;
	FrameLayout frameLayout;
	View addCity;
	GregorianCalendar updated = new GregorianCalendar();
	
	boolean notificated = false;

	public boolean isNotificated() {
		return notificated;
	}

	public void setNotificated(boolean notificated) {
		this.notificated = notificated;
	}

	public GregorianCalendar getUpdated() {
		return updated;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_city);
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}

		getFragments();
		getViews();
		changeFragment(0);
	}

	private void getViews() {
		// Set up the ViewPager with the sections adapter.
		frameLayout = (FrameLayout) findViewById(R.id.container);
		lblCityName = (TextView) findViewById(R.id.lblCityName);
		lblLastUpdate = (TextView) findViewById(R.id.lblLastUpdate);
		addCity = findViewById(R.id.addCity);
		addCity.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Add city dialog
				AlertDialog.Builder builder = new AlertDialog.Builder(
						CityActivity.this);
				builder.setTitle(getString(R.string.add_a_city));

				// Set up the input
				final EditText input = new EditText(CityActivity.this);
				// Specify the type of input expected; this, for example, sets
				// the input as a password, and will mask the text
				builder.setView(input);

				// Set up the buttons
				builder.setPositiveButton(getString(R.string.ok),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								String newCity = input.getText().toString();
								if(TextUtils.isEmpty(newCity)){
									dialog.cancel();
									return;
								}
								fragments.add(new CityFragment(newCity));
								changeFragment(fragments.size() - 1);

							}
						});
				builder.setNegativeButton(getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.cancel();
							}
						});

				builder.show();

			}
		});
		citySelector = (LinearLayout) findViewById(R.id.citySelector);
		citySelector.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final String[] items = new String[fragments.size()];
				for (int i = 0; i < fragments.size(); i++) {
					items[i] = fragments.get(i).getCityName();
				}
				// Launch spinner dialog
				AlertDialog.Builder builder = new AlertDialog.Builder(
						CityActivity.this);
				// builder.setTitle(R.string.pick_color)
				builder.setItems(items, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// The 'which' argument contains the index position
						// of the selected item
						changeFragment(which);
					}
				});
				builder.create().show();
			}
		});
	}

	private void getFragments() {
		String[] cities = getResources().getStringArray(R.array.cities_ex);
		for(String c:cities){
			fragments.add(new CityFragment(c));
		}
	}

	public CityFragment getVisibleCityFragment() {
		return fragments.get(visiblePosition);
	}

	/**
	 * @param which
	 */
	private void changeFragment(int position) {
		visiblePosition = position;
		// Change fragment
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.replace(R.id.container, fragments.get(position));
		ft.setTransition(FragmentTransaction.TRANSIT_NONE);
		ft.commit();
		lblCityName.setText(fragments.get(position).getCityName());
//		updated.set(115, 6, 14, 22, 01);// TEST
		String lastUpdate = getString(R.string.last_update);
		lblLastUpdate.setText(lastUpdate.replace("TIME", Hours.hhmm(updated)));
	}

	@Override
	public void onBackPressed() {
		if (getVisibleCityFragment().isShowingDetail()) {
			// Hide detail
			getVisibleCityFragment().toggleDetail(false);
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.city, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_about) {
			About.show(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_city, container,
					false);
			return rootView;
		}
	}
}
