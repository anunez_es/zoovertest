/**
 * 
 */
package com.anunez.zoovertest.about;

import com.anunez.zoovertest.R;

import android.app.Activity;
import android.app.AlertDialog;

/**
 * @author Angel
 *
 */
public class About {
	
	public static void show(Activity activity){
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle(R.string.app_name);
		builder.setMessage(R.string.about_message);
		builder.setIcon(R.drawable.ic_launcher);
		builder.setNeutralButton(R.string.close, null);
		builder.show();
		
	}

}
