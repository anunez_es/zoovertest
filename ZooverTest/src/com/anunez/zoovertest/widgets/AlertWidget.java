/**
 * 
 */
package com.anunez.zoovertest.widgets;

import java.util.List;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.RingtoneManager;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.anunez.zoovertest.CityActivity;
import com.anunez.zoovertest.R;
import com.anunez.zoovertest.util.Utils.Hours;
import com.anunez.zoovertest.weather.Weather;
import com.anunez.zoovertest.weather.Weather.Condition;

/**
 * @author Angel
 *
 */
public class AlertWidget extends Widget {

	private TextView lblAlert;
	private ImageView imgAlert;
	private CityActivity activity;
	private boolean flashed = false;

	public AlertWidget(Context context, List<Weather> weathers) {
		super(context, weathers);
		layout_id = R.layout.widget_3;
		activity = (CityActivity) getContext();
	}

	@Override
	public void inflate() {
		super.inflate();

		lblAlert = (TextView) findViewById(R.id.lblAlert);
		imgAlert = (ImageView) findViewById(R.id.imgAlert);

		// Search for rain forecast
		int firstRain = -1, lastRain = -1;
		for (Weather w : weathers) {
			if (w.getCondition() == Condition.RAINY) {// Found rain
				if (firstRain == -1) {// Found first rain
					firstRain = w.getHour();
					lastRain = w.getHour();
				} else {// Second (or more) consecutive rain
					lastRain = w.getHour();
				}
			} else if (firstRain != -1) {// Found non-rain after rain
				break;
			}
		}

		if (firstRain != -1) {// Rain found
			prepareWidget(firstRain, lastRain);
			// Program notification
			if (!activity.isNotificated()) {
				programNotification(firstRain);
			}
		} else {// Rain not found
			lblAlert.setText(activity.getString(R.string.alert_no));
			imgAlert.setVisibility(View.GONE);
		}
	}

	/**
	 * Fills the text and the image in the widget. Also will make the widget
	 * flash if there is a rain alert.
	 * 
	 * @param context
	 * @param firstRain
	 * @param lastRain
	 */
	private void prepareWidget(int firstRain, int lastRain) {

		String alert = activity.getString(R.string.alert_rain_ex);
		int alert_duration = (lastRain + 1) - firstRain;
		int hour_string = alert_duration > 1 ? R.string.hours : R.string.hour;
		String duration = alert_duration + " "
				+ activity.getString(hour_string);
		String start = Hours.hour(firstRain);
		alert = alert.replace("DURATION", "" + duration)
				.replace("START", start);
		lblAlert.setText(alert);
		imgAlert.setVisibility(View.VISIBLE);
		imgAlert.setImageResource(R.drawable.rainy);

		// Flash the widget a few times and keep dark
		if (!flashed) {
			final Resources res = activity.getResources();
			for (int i = 0; i < 7; i++) {
				final int f = i;
				Runnable runnable = new Runnable() {
					@Override
					public void run() {
						int bgColor = 0, textColor = 0;
						if (f % 2 == 0) {
							bgColor = res.getColor(R.color.dark_blue);
							textColor = res.getColor(R.color.pale_blue);
						} else {
							bgColor = res.getColor(R.color.white);
							textColor = res.getColor(R.color.black);
						}
						setBackgroundColor(bgColor);
						lblAlert.setTextColor(textColor);
					}
				};
				postDelayed(runnable, 800 + 200 * i);
				// Start after a certain time
			}
			flashed = true;
		}
	}

	/**
	 * Programs a notification.
	 * 
	 * @param context
	 * @param firstRain
	 */
	private void programNotification(int firstRain) {
		// Just one notification per session
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager nm = (NotificationManager) activity
				.getSystemService(ns);

		Notification.Builder nb = new Notification.Builder(activity);
		int icon = R.drawable.rainy;
		int when = firstRain;
		String city = activity.getVisibleCityFragment().getCityName();
		CharSequence tickerText = activity.getString(R.string.weather_alert)
				+ city;
		CharSequence contentTitle = tickerText;
		CharSequence contentText = lblAlert.getText();
		nb.setTicker(tickerText);
		nb.setContentTitle(contentTitle);
		nb.setContentText(contentText);
		nb.setSmallIcon(icon);
		Uri alarmSound = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		nb.setSound(alarmSound);

		Intent notificationIntent = new Intent(activity, CityActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);

		PendingIntent intent = PendingIntent.getActivity(activity, 0,
				notificationIntent, 0);
		nb.setContentIntent(intent);

		Notification notif = nb.build();
		notif.flags |= Notification.FLAG_AUTO_CANCEL;
		nm.notify(when, notif);
		activity.setNotificated(true);
	}
}
