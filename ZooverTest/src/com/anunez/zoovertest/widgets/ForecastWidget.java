/**
 * 
 */
package com.anunez.zoovertest.widgets;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anunez.zoovertest.R;
import com.anunez.zoovertest.weather.Weather;

/**
 * @author Angel
 *
 */
public class ForecastWidget extends Widget {

	public ForecastWidget(Context context, List<Weather> weathers) {
		super(context, weathers);
		layout_id = R.layout.widget_2;
	}

	@Override
	public void inflate() {
		super.inflate();
		final LinearLayout weatherPerHour = (LinearLayout) findViewById(R.id.weatherPerHour);
		weatherPerHour.removeAllViewsInLayout();

		for (Weather w : weathers) {
			// Inflate
			LayoutInflater inflater = LayoutInflater.from(getContext());
			LinearLayout small = (LinearLayout) inflater.inflate(
					R.layout.weather_small, this, false);
			TextView lblTime = (TextView) small.findViewById(R.id.lblTime);
			int hour = w.getHour() % 24;
			lblTime.setText((hour < 10 ? "0" : "") + hour + ":00");
			// One-digit hour support
			ImageView imgCondition = (ImageView) small
					.findViewById(R.id.imgCondition);
			imgCondition.setImageResource(w.getDrawableResource());
			TextView lblTemp = (TextView) small.findViewById(R.id.lblTemp);
			lblTemp.setText(w.getTemperature() + " �C");

			weatherPerHour.addView(small);
		}
		AnimationListener animationListener = new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				Animation marquee = AnimationUtils.loadAnimation(
						getContext(), R.anim.marquee);
				weatherPerHour.startAnimation(marquee);
			}
		};
		
		Animation start = AnimationUtils.loadAnimation(
				getContext(), R.anim.marquee_start);
		start.setAnimationListener(animationListener);
		weatherPerHour.startAnimation(start);

	}

}
