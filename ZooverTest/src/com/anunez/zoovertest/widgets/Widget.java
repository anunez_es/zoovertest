/**
 * 
 */
package com.anunez.zoovertest.widgets;

import java.util.List;

import com.anunez.zoovertest.weather.Weather;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

/**
 * @author Angel
 *
 */
public abstract class Widget extends RelativeLayout implements Comparable<Widget>{
	
	/** Used to make widget order persistent. */
	int position;
	List<Weather> weathers;
	
	public Widget(Context context, List<Weather> weathers) {
		super(context);
		this.weathers = weathers;
	}

	protected int layout_id;

	public int getLayout_id() {
		return layout_id;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void inflate() {
		LayoutInflater inflater = LayoutInflater.from(getContext());
		inflater.inflate(layout_id, this);
	}

	@Override
	public int compareTo(Widget another) {		
		return position-another.getPosition();
	}
	
	

}
