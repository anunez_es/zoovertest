/**
 * 
 */
package com.anunez.zoovertest.widgets;

import java.util.List;

import android.content.Context;
import android.widget.TextView;

import com.anunez.zoovertest.R;
import com.anunez.zoovertest.custom.GifDecoderView;
import com.anunez.zoovertest.weather.Weather;

/**
 * @author Angel
 *
 */
public class NowWidget extends Widget {

	private static final int LAYOUT = R.layout.widget_1;
	private GifDecoderView gifCondition;

	public NowWidget(Context context, List<Weather> weathers) {
		super(context, weathers);
		layout_id = LAYOUT;

		/*
		 * Not possible to set OnClickListener here. Not compatible with
		 * drag&drop of the widget in the list.
		 */

	}

	@Override
	public void inflate() {
		super.inflate();

		Weather w = weathers.get(0);// First (current)
		
		TextView lblTemp = (TextView) findViewById(R.id.lblTemp);
		lblTemp.setText(w.getTemperature() + "�");
		gifCondition = (GifDecoderView) findViewById(R.id.webCondition);
		refresh();
	}
	
	public void refresh(){
		Weather current = weathers.get(0);// First (current)
		gifCondition.setAnimatedResource(current.getAnimatedResource());
	}

}
