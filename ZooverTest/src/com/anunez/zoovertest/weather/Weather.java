/**
 * 
 */
package com.anunez.zoovertest.weather;

import java.util.Random;

import com.anunez.zoovertest.R;

/**
 * @author Angel
 *
 */
public class Weather {
	public enum Condition {
		SUNNY, CLOUDY, RAINY
	};

	static Random random = new Random();
	static final int[] DRAWABLE_RESOURCES = new int[] { R.drawable.sunny,
			R.drawable.cloudy, R.drawable.rainy };
	static final int[] ANIMATED_RESOURCES = new int[] { R.drawable.sunny_a,
		R.drawable.cloudy_a, R.drawable.rainy_a };
	static final String[] ANIMATED_ASSETS = new String[] { "sunny_a.gif",
		"cloudy_a.gif", "rainy_a.gif" };
	protected Condition condition;
	protected int hour;
	private int temperature;
	protected int drawableResource;
	private int animatedResource;
	String animatedAsset;

	public int getDrawableResource() {
		return drawableResource;
	}

	public int getAnimatedResource() {
		return animatedResource;
	}

	public String getAsset() {
		return animatedAsset;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public static Weather generate(int hour) {
		Weather weather = new Weather();
		int conditionInt = random.nextInt(Condition.values().length);
		weather.condition = Condition.values()[conditionInt];
		weather.drawableResource = DRAWABLE_RESOURCES[conditionInt];
		weather.animatedResource = ANIMATED_RESOURCES[conditionInt];
		weather.animatedAsset = ANIMATED_ASSETS[conditionInt];
		weather.hour = hour;
		int MIN_TEMP = 15, MAX_TEMP = 28;
		weather.temperature = MIN_TEMP + random.nextInt(MAX_TEMP - MIN_TEMP);

		return weather;
	}

	public Condition getCondition() {
		return condition;
	}

	public int getTemperature() {
		return temperature;
	}

}
