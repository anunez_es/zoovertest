/**
 * 
 */
package com.anunez.zoovertest.custom;

import java.io.InputStream;

import jp.tomorrowkey.android.gifplayer.GifDecoder;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Gif decoder view. This view plays an animated GIF cinema style, playing every
 * frame of it sequentially.
 * 
 * @author Angel
 *
 */
public class GifDecoderView extends ImageView {
	InputStream is;

	private boolean mIsPlayingGif = false;
	private GifDecoder mGifDecoder;
	private Bitmap mTmpBitmap;
	final Handler mHandler = new Handler();
	final Runnable mUpdateResults = new Runnable() {
		public void run() {
			if (mTmpBitmap != null && !mTmpBitmap.isRecycled()) {
				GifDecoderView.this.setImageBitmap(mTmpBitmap);
			}
		}
	};

	// http://droid-blog.net/2011/10/15/tutorial-how-to-play-animated-gifs-in-android-%e2%80%93-part-2/#sthash.0CFIHSWJ.dpuf

	public GifDecoderView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setAnimatedResource(int resource) {
		is = getContext().getResources().openRawResource(resource);
		playGif(is);
	}

	private void playGif(InputStream stream) {
		mGifDecoder = new GifDecoder();
		mGifDecoder.read(stream);
		mIsPlayingGif = true;
		new Thread(new Runnable() {
			public void run() {
				final int n = mGifDecoder.getFrameCount();
				final int ntimes = mGifDecoder.getLoopCount();
				int repetitionCounter = 0;
				do {
					for (int i = 0; i < n; i++) {
						try {
							mTmpBitmap = mGifDecoder.getFrame(i);
							final int t = mGifDecoder.getDelay(i);
							mHandler.post(mUpdateResults);
							Thread.sleep(t);
						} catch (Exception e) {
						}
					}
					if (ntimes != 0) {
						repetitionCounter++;
					}
				} while (mIsPlayingGif && (repetitionCounter <= ntimes));
			}
		}).start();
	}
}
