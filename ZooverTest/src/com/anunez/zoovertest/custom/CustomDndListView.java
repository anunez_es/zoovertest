/**
 * 
 */
package com.anunez.zoovertest.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ListView;

import com.anunez.zoovertest.CityActivity;
import com.anunez.zoovertest.adapters.DragNDropWidgetAdapter;
import com.anunez.zoovertest.widgets.NowWidget;
import com.terlici.dragndroplist.DragNDropListView;

/**
 * Custom Drag and Drop {@link ListView} for this app. Extends
 * {@link DragNDropListView}.
 * 
 * @author Angel
 *
 */
public class CustomDndListView extends DragNDropListView {

	public CustomDndListView(Context context) {
		super(context);
	}

	public CustomDndListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public CustomDndListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	int startX, startY;

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		final int action = ev.getAction();
		final int x = (int) ev.getX();
		final int y = (int) ev.getY();

		if (action == MotionEvent.ACTION_DOWN && isDrag(ev))
			mDragMode = true;

		if (!mDragMode || !isDraggingEnabled)
			return super.onTouchEvent(ev);

		switch (action) {
		case MotionEvent.ACTION_DOWN:
			startX = x;
			startY = y;
			mStartPosition = pointToPosition(x, y);

			if (mStartPosition != INVALID_POSITION) {
				int childPosition = mStartPosition - getFirstVisiblePosition();
				mDragPointOffset = y - getChildAt(childPosition).getTop();
				mDragPointOffset -= ((int) ev.getRawY()) - y;

				startDrag(childPosition, y);
				drag(0, y);
			}

			break;
		case MotionEvent.ACTION_MOVE:
			drag(0, y);
			break;
		case MotionEvent.ACTION_CANCEL:
		case MotionEvent.ACTION_UP:
		default:
			mDragMode = false;

			if (mStartPosition != INVALID_POSITION) {
				DragNDropWidgetAdapter adapter = (DragNDropWidgetAdapter) getAdapter();
				boolean expand = false;
				// Need to copy the index. mStartPosition is reset
				int widgetPosition = mStartPosition;
				// check if the position is a header/footer
				Log.i("Positions", +startX + "," + startY + " ; " + x + "," + y);
				int TOLERANCE = 5;// Click tolerance
				if (getContext() instanceof CityActivity
						&& Math.abs(x - startX) <= TOLERANCE
						&& Math.abs(y - startY) <= TOLERANCE) {

					Log.i("Positions", "Start: " + widgetPosition + "/"
							+ adapter.getItem(widgetPosition).getClass().getSimpleName());
					expand = true;
				}
				int actualPosition = pointToPosition(x, y);
				if (actualPosition > (getCount() - getFooterViewsCount()) - 1)
					actualPosition = INVALID_POSITION;

				// mStartPosition is reset
				stopDrag(mStartPosition - getFirstVisiblePosition(),
						actualPosition);
				if (expand) {// Expand info
					CityActivity activity = (CityActivity) getContext();
					Log.d("NowWidget", "Click");
					if (getAdapter() instanceof DragNDropWidgetAdapter) {
						/* If is NowWidget, show detail
						 * Important: Use widgetPosition, 
						 * NOT adapter.getPositionsArray[widgetPosition]
						 */
						if (adapter.getItem(widgetPosition) instanceof NowWidget) {
							activity.getVisibleCityFragment()
									.toggleDetail(true);
						}
					}
					return true;
				}
			}
			break;
		}

		return true;
	}

}
