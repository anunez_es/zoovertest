/**
 * 
 */
package com.anunez.zoovertest.fragments;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anunez.zoovertest.CityActivity;
import com.anunez.zoovertest.R;
import com.anunez.zoovertest.adapters.DragNDropWidgetAdapter;
import com.anunez.zoovertest.custom.CustomDndListView;
import com.anunez.zoovertest.custom.GifDecoderView;
import com.anunez.zoovertest.weather.Weather;
import com.anunez.zoovertest.widgets.AlertWidget;
import com.anunez.zoovertest.widgets.ForecastWidget;
import com.anunez.zoovertest.widgets.NowWidget;
import com.anunez.zoovertest.widgets.Widget;
import com.terlici.dragndroplist.DragNDropListView;

/**
 * @author Angel
 *
 */
@SuppressWarnings("unused")
public class CityFragment extends Fragment {

	public static final int N_WIDGETS = 3;
	String cityName;
	CityActivity activity;

	View rootview;

	// Detail views
	RelativeLayout rlDetail;
	View closeDetail;
	TextView lblTemp, lblMinMax, lblConditionDescription, lblWindSpeed;
	ImageView imgCondition;
	GifDecoderView gifCondition;

	// List views
	CustomDndListView lstWidgets;
	List<Widget> widgets = new ArrayList<Widget>();
	DragNDropWidgetAdapter adapter;

	List<Weather> weathers = new ArrayList<Weather>();

	public CityFragment(String cityName) {
		super();
		this.cityName = cityName;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		activity = (CityActivity) getActivity();
		rlDetail = (RelativeLayout) rootview.findViewById(R.id.rlDetail);
		rlDetail.setVisibility(View.GONE);
		getDetailViews();

		lstWidgets = (CustomDndListView) rootview.findViewById(R.id.lstWidgets);
		lstWidgets.setVisibility(View.VISIBLE);
		Log.i(cityName, "Widgets: " + widgets.size());
		if (widgets.size() == 0) {
			// Create weather
			int currentHour = activity.getUpdated().get(Calendar.HOUR_OF_DAY);
			for (int i = 0; i < 5; i++) {
				weathers.add(Weather.generate(currentHour + i));
			}

			// Create widgets
			widgets.add(new NowWidget(activity, weathers));
			widgets.add(new ForecastWidget(activity, weathers));
			widgets.add(new AlertWidget(activity, weathers));
			Log.i(cityName, "Widgets: " + widgets.size());

		}
		fillDetail();

		// Prepare adapter
		Collections.sort(widgets);// Reload custom order
		adapter = new DragNDropWidgetAdapter(activity, widgets, 0,
				R.id.widget_container);
		lstWidgets.setDragNDropAdapter(adapter);
	}

	private void getDetailViews() {
		closeDetail = rlDetail.findViewById(R.id.closeDetail);
		closeDetail.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				toggleDetail(false);
			}
		});
		lblTemp = (TextView) rlDetail.findViewById(R.id.lblTemp);
		lblMinMax = (TextView) rlDetail.findViewById(R.id.lblMinMax);
		lblConditionDescription = (TextView) rlDetail
				.findViewById(R.id.lblConditionDescription);
		lblWindSpeed = (TextView) rlDetail.findViewById(R.id.lblWindSpeed);
		imgCondition = (ImageView) rlDetail.findViewById(R.id.imgCondition);
		gifCondition = (GifDecoderView) rlDetail.findViewById(R.id.webCondition);
		gifCondition.setVerticalScrollBarEnabled(false);
		gifCondition.setHorizontalScrollBarEnabled(false);
	}

	private void fillDetail() {
		Weather current = weathers.get(0);
		lblTemp.setText(current.getTemperature() + "�");
		int min = current.getTemperature(), max = current.getTemperature();
		for (Weather w : weathers) {
			if (w.getTemperature() < min)
				min = w.getTemperature();
			if (w.getTemperature() > max)
				max = w.getTemperature();
		}
		lblMinMax.setText(getString(R.string.temp_minmax_ex).replace("MIN",
				String.valueOf(min)).replace("MAX", String.valueOf(max)));
		String[] conditions = getResources().getStringArray(R.array.conditions);
		lblConditionDescription.setText(conditions[current.getCondition()
				.ordinal()]);
		// imgCondition.setImageResource(current.getDrawableResource());
		reloadGifView(current);
	}

	/**
	 * @param current
	 */
	private void reloadGifView(Weather current) {
		gifCondition.setAnimatedResource(current.getAnimatedResource());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// return super.onCreateView(inflater, container, savedInstanceState);
		rootview = inflater.inflate(R.layout.fragment_city, container, false);
		return rootview;
	}

	public void toggleDetail(boolean detail) {
		rlDetail.setVisibility(detail ? View.VISIBLE : View.GONE);
		lstWidgets.setVisibility(detail ? View.GONE : View.VISIBLE);
	}

	public boolean isShowingDetail() {
		return rlDetail.getVisibility() == View.VISIBLE;
	}

	public String getCityName() {
		return cityName;
	}

	public List<Widget> getWidgets() {
		return widgets;
	}

}
